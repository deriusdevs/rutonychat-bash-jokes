using System.IO;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Net.Http;
using System.Xml;
using System.ServiceModel.Syndication;
using System.Text.RegularExpressions;

namespace RutonyChat {
    public class Script {
        public void RunScript(string site, string username, string text, string param) {

        	string url = "http://bash.im/rss/";
        	string curentDate = DateTime.Now.ToString("dd/MM/yy");
        	string file = ProgramProps.dir_scripts + @"\bot_evgenius_bash.txt";

        	if(!File.Exists(file))
        	{
        		addJoke(url, file, site, curentDate);
        	}
        	else
        	{
        		string[] fileDate = File.ReadAllLines(file);

        		DateTime dateF = DateTime.Parse(fileDate[0]);
        		DateTime dateN = DateTime.Now;

        		if(dateF.Date != dateN.Date)
				{
					File.Delete(file);
					addJoke(url, file, site, curentDate);
				}
        	}

        	string[] jokes = File.ReadAllLines(file);
        	int jokesCount = jokes.Length-1;

        	Random rnd = new Random();

        	int rJoke = rnd.Next(1, jokesCount);

        	RutonyBot.BotSay(site, jokes[rJoke]);
		}

		public void addJoke(string url, string file, string site, string date)
		{
        	using (XmlReader reader = XmlReader.Create(url))
        	{
        		SyndicationFeed feed = SyndicationFeed.Load(reader);

        		RutonyBotFunctions.FileAddString(file, date);

        		foreach(SyndicationItem item in feed.Items)
        		{
        			string bash_message = item.Summary.Text;
        			bash_message = Regex.Replace(bash_message, "<.*?>", " ");
        			bash_message = Regex.Replace(bash_message, "&lt;", String.Empty);
        			bash_message = Regex.Replace(bash_message, "&gt;", String.Empty);
        			bash_message = Regex.Replace(bash_message, "&quot;", String.Empty);

        			if(bash_message.Length <= 400)
        			{
        				RutonyBotFunctions.FileAddString(file, bash_message);
        			}
        		}
        	}
		}
    }
}